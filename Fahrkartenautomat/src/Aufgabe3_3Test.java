import java.util.Scanner;

	class Aufgabe3_3Test {
		static Scanner tastatur = new Scanner(System.in);
		
	    public static void main(String[] args) {

	        double zuZahlenderBetrag = fahrkartenbestellungErfassen(), r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

	        fahrkartenAusgeben();
	        
	        rueckgeldAusgeben(r�ckgabebetrag);
	        
	        tastatur.close();

	        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
	                "vor Fahrtantritt entwerten zu lassen!\n" +
	                "Wir w�nschen Ihnen eine gute Fahrt.");
	    }
	    
	    public static double fahrkartenbestellungErfassen() {
	        double zuZahlenderBetrag;
	        int anzahlTickets;
	        
	        System.out.print("Ticketpreis (EURO): ");
	        zuZahlenderBetrag = tastatur.nextDouble();
	        
	        System.out.print("Ticket Anzahl: ");
	        // Wert zuweisung des Integers.
	        anzahlTickets = tastatur.nextInt();
	        	
	        	if (anzahlTickets <=0 || anzahlTickets >10){  // || == oder
	        		System.out.print("Die eingegebene Ticketzahl ist nicht Valide, Ticketanzahl wird zu 1 ge�ndert.\n");
	        		anzahlTickets = 1;
	        	}
	        	
	        		else {
	        			System.out.print("Die eingegebene Ticketzahl ist " + anzahlTickets + ". Bitte Warten!\n");
		        	}
	        
	        
	        // Das bearbeiten des end Betrags.
	        return zuZahlenderBetrag *anzahlTickets;	
	    }
	    
	    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
	        // Geldeinwurf
	        // -----------
	        double eingezahlterGesamtbetrag = 0.0;
	        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
	            System.out.println("Noch zu zahlen: " + String.format("%.2f Euro", (zuZahlenderBetrag - eingezahlterGesamtbetrag)));
	            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	            double eingeworfeneMuenze = tastatur.nextDouble();
	            if eingeworfeneMuenze <2;
	            
	            eingezahlterGesamtbetrag += eingeworfeneMuenze;
	        }
	        
	        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	    }
	    
	    public static void fahrkartenAusgeben() {
	        // Fahrscheinausgabe
	        // -----------------
	        System.out.println("\nFahrschein wird ausgegeben");
	        for (int i = 0; i < 8; i++) {
	            System.out.print("=");
	            try {
	                Thread.sleep(250);
	            } catch (InterruptedException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	        }
	        System.out.println("\n\n");
	    }
	    
	    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
	        // R�ckgeldberechnung und -Ausgabe
	        // -------------------------------
	        
	        if (r�ckgabebetrag > 0.0) {
	            System.out.println("Der R�ckgabebetrag in H�he von " + String.format("%.2f Euro", r�ckgabebetrag));
	            System.out.println("wird in folgenden M�nzen ausgezahlt:");

	            int zweiEuro = 0, einEuro = 0, fuenfzigCent = 0, zwanzigCent = 0, zehnCent = 0, fuenfCent = 0;

	            while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	            {
	                zweiEuro++;
	                r�ckgabebetrag -= 2.0;
	            }
	            while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	            {
	                einEuro++;
	                r�ckgabebetrag -= 1.0;
	            }
	            while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	            {
	                fuenfzigCent++;
	                r�ckgabebetrag -= 0.5;
	            }
	            while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	            {
	                zwanzigCent++;
	                r�ckgabebetrag -= 0.2;
	            }
	            while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	            {
	                zehnCent++;
	                r�ckgabebetrag -= 0.1;
	            }
	            while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	            {
	                fuenfCent++;
	                r�ckgabebetrag -= 0.05;
	            }

	            if (zweiEuro > 0) System.out.println(zweiEuro + " mal 2 Euro St�ck" + (zweiEuro > 1 ? "e" : "") + ".");
	            if (einEuro > 0) System.out.println(einEuro + " mal 1 Euro St�ck" + (einEuro > 1 ? "e" : "") + ".");
	            if (fuenfzigCent > 0) System.out.println(fuenfzigCent + " mal 50 Cent St�ck" + (fuenfzigCent > 1 ? "e" : "") + ".");
	            if (zwanzigCent > 0) System.out.println(zwanzigCent + " mal 20 Cent St�ck" + (zwanzigCent > 1 ? "e" : "") + ".");
	            if (zehnCent > 0) System.out.println(zehnCent + " mal 10 Cent St�ck" + (zehnCent > 1 ? "e" : "") + ".");
	            if (fuenfCent > 0) System.out.println(fuenfCent + " mal 5 Cent St�ck" + (fuenfCent > 1 ? "e" : "") + ".");

	        }
	    }
	}
	